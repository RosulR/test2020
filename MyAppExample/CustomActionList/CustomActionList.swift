//
//  CustomActionList.swift
//  MyAppExample
//
//  Created by Roman Rosul on 30/11/20.
//

import UIKit

class CustomActionListViewController: UIViewController {
    
    struct Constants {
        static let screenHeight = UIScreen.main.bounds.size.height
        static let defaultItemHeight: CGFloat = 26
        static let defaultItemSpacing: CGFloat = 22
        static let itemHorizontalOffset: CGFloat = 16
        static let bottomOffset: CGFloat = 100
    }
    
    var actions: [CustomActionListItem]!
    var bottomConstraint: NSLayoutConstraint!
    
    // MARK: - Lifecycle
    
    override func loadView() {
        self.createMainView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        overrideUserInterfaceStyle = .light
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        view.addGestureRecognizer(tap)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        bottomConstraint.constant = Constants.bottomOffset
        UIView.animate(withDuration: 0.5) { [weak self] in
            self?.view.layoutIfNeeded()
        }
    }
    
    // MARK: - Actions
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func didTapItem(_ sender: UIButton? = nil) {
        self.dismiss(animated: true) { [weak self] in
            if let index = sender?.tag {
                self?.actions[index].handler?()
            }
        }
    }
    
    // MARK: - Other
    
    func createMainView() {
        view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        
        let containerView = UIView()
        containerView.backgroundColor = .white
        containerView.layer.cornerRadius = 8
        view.addSubview(containerView)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        containerView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
        let currentHeight = CGFloat(actions.count) * (Constants.defaultItemHeight + Constants.defaultItemSpacing) + Constants.defaultItemSpacing + Constants.bottomOffset
        let containerHeight = min((Constants.screenHeight / 1.5), currentHeight);
        
        containerView.heightAnchor.constraint(equalToConstant: containerHeight).isActive = true
        bottomConstraint = containerView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        bottomConstraint.isActive = true
        bottomConstraint.constant = Constants.screenHeight
        
        let scrollView = UIScrollView()
        containerView.addSubview(scrollView)
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
        scrollView.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
        scrollView.heightAnchor.constraint(equalTo: containerView.heightAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: containerView.widthAnchor).isActive = true
        
        let stackView = UIStackView()
        stackView.axis = .vertical
        
        scrollView.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: Constants.itemHorizontalOffset).isActive = true
        stackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -Constants.itemHorizontalOffset).isActive = true
        stackView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: Constants.defaultItemSpacing).isActive = true
        stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
        
        for action in actions {
            let view = self.createItemViewFor(action)
            stackView.addArrangedSubview(view)
        }
    }
    
    func createItemViewFor(_ action: CustomActionListItem) -> UIView {
        let container = UIView()
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = action.icon.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = action.iconColor
        container.addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.leadingAnchor.constraint(equalTo: container.leadingAnchor).isActive = true
        imageView.topAnchor.constraint(equalTo: container.topAnchor).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 24).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 24).isActive = true
        
        let label = UILabel()
        label.text = action.title
        label.font = UIFont.systemFont(ofSize: 17, weight: .medium)
        container.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 24).isActive = true
        label.trailingAnchor.constraint(equalTo: container.trailingAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: imageView.centerYAnchor).isActive = true
        let button = UIButton()
        container.addSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.leadingAnchor.constraint(equalTo: container.leadingAnchor).isActive = true
        button.trailingAnchor.constraint(equalTo: container.trailingAnchor).isActive = true
        button.topAnchor.constraint(equalTo: container.topAnchor).isActive = true
        button.bottomAnchor.constraint(equalTo: container.bottomAnchor).isActive = true
        button.addTarget(self, action: #selector(didTapItem(_:)), for: .touchUpInside)
        if let index = actions.firstIndex(where: {$0 === action}) {
            button.tag = index
        }
        container.heightAnchor.constraint(equalToConstant: Constants.defaultItemHeight + Constants.defaultItemSpacing).isActive = true
        return container
    }
    
}
