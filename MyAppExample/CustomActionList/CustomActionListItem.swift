//
//  CustomActionListItem.swift
//  MyAppExample
//
//  Created by Roman Rosul on 30/11/20.
//

import UIKit
typealias EmptyCompletion = () -> Void

class CustomActionListItem {
    
    let title: String
    let icon: UIImage
    let iconColor: UIColor
    let handler: EmptyCompletion?
    
    init(title: String, icon: UIImage, color: UIColor, handler: EmptyCompletion?) {
        self.title = title
        self.icon = icon
        self.iconColor = color
        self.handler = handler
    }
    
    init(_ type: ActionType, handler: EmptyCompletion?) {
        self.title = type.title
        self.icon = type.icon
        self.iconColor = type.iconTintColor
        self.handler = handler
    }
    
}
