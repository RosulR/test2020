//
//  ViewController.swift
//  MyAppExample
//
//  Created by Roman Rosul on 30/11/20.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet private weak var choiseLabel: UILabel!
    @IBOutlet private weak var mainButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        overrideUserInterfaceStyle = .light
        choiseLabel.isHidden = true
    }
    
    @IBAction private func mainButtonTap(_ sender: Any) {
        choiseLabel.isHidden = true
        
        let controller = CustomActionListViewController()
        controller.modalPresentationStyle = .overFullScreen
        controller.modalTransitionStyle = .crossDissolve
        
        let firstAction = CustomActionListItem.init(.edit) {
            self.choiseLabel.isHidden = false
            self.choiseLabel.text = "Selected: Edit"
        }
        
        let secondAction = CustomActionListItem.init(.markUncomplete) {
            self.choiseLabel.isHidden = false
            self.choiseLabel.text = "Selected: Mark as Uncomplete"
        }
        
        let thirdAction = CustomActionListItem.init(.markUnpaid) {
            self.choiseLabel.isHidden = false
            self.choiseLabel.text = "Selected: Mark as Unpaid"
        }
        
        let fourthAction = CustomActionListItem.init(.viewNotes) {
            self.choiseLabel.isHidden = false
            self.choiseLabel.text = "Selected: Edit / View Notes"
        }
        
        let fifthAction = CustomActionListItem.init(.delete) {
            self.choiseLabel.isHidden = false
            self.choiseLabel.text = "Selected: Delete"
        }
        
//        let customAction = CustomActionListItem.init(title: "Custom Action", icon: #imageLiteral(resourceName: "ic_custom"), color: .green) {
//                    self.choiseLabel.isHidden = false
//                    self.choiseLabel.text = "Well done :)"
//                }
        
        controller.actions = [/*customAction,*/ firstAction, secondAction, thirdAction, fourthAction, fifthAction]
        
        show(controller, sender: sender)
    }
    
}

