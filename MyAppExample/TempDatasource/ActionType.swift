//
//  ActionType.swift
//  MyAppExample
//
//  Created by Roman Rosul on 30/11/20.
//

import UIKit

enum ActionType {
    case edit
    case markUncomplete
    case markUnpaid
    case viewNotes
    case delete
    
    
    var title: String {
        switch self {
        case .edit:
            return NSLocalizedString("Edit", comment: "")
        case .markUncomplete:
            return NSLocalizedString("Mark as Uncomplete", comment: "")
        case .markUnpaid:
            return NSLocalizedString("Mark as Unpaid", comment: "")
        case .viewNotes:
            return NSLocalizedString("Edit / View Notes", comment: "")
        case .delete:
            return NSLocalizedString("Delete", comment: "")
        }
    }
    
    var icon: UIImage {
        switch self {
        case .edit:
            return #imageLiteral(resourceName: "ic_edit")
        case .markUncomplete:
            return #imageLiteral(resourceName: "ic_uncomplete")
        case .markUnpaid:
            return #imageLiteral(resourceName: "ic_unpaid")
        case .viewNotes:
            return #imageLiteral(resourceName: "ic_notes")
        case .delete:
            return #imageLiteral(resourceName: "ic_delete")
        }
    }
    
    var iconTintColor: UIColor {
        switch self {
        case .edit, .viewNotes, .delete:
            return .black
        case .markUncomplete, .markUnpaid:
            return .red
        }
    }
    
}
